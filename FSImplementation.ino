#include <EEPROM.h>

#define RESERVED_BLOCKS_LEN 16
#define TOTAL_BLOCKS 256
#define BLOCK_SIZE_BYTES 16
//#define EMULATE_EEPROM true
#define EOF_FLAG 1

// first 16 blocks not available for files
#define FILE_DESCRIPTOR_SIZE 4
#define FREE_BLOCK_LINKED_LIST_BLOCK_HEAD_LOC 0
#define FREE_BLOCK_LINKED_LIST_BLOCK_TAIL_LOC 1
#define ROOT_FILE_DESCRIPTOR_LOC 2
#define ROOT_FD_NAME '$'

// File integer format: first 8 bits is the location of the file 
// descriptor block and imediately following 4 bits is the offset 
// within the FAT block
typedef uint16_t FileInteger;

const int USER_BLOCKS = TOTAL_BLOCKS - RESERVED_BLOCKS_LEN;

struct FileDescriptor {
  unsigned char file_name;
  uint8_t first_block = EOF_FLAG;
  // 16 bits: F bit for whether it is a directory, everything else is size
  uint16_t directory_plus_size = 0;
};

// TODO HW: Get file reading working

struct FileHandle {
  uint16_t file_byte_pos = 0;
  // current block position of the file_byte_pos
  uint8_t current_file_pos_block_num = 0;
  uint8_t current_file_first_block_num = EOF_FLAG;
  uint8_t current_block_data_cache[BLOCK_SIZE_BYTES];
  bool block_modified = false;
  FileInteger file_integer = 0;
  uint16_t file_size = 0;
  bool open = false;
};

namespace descriptor_utils {
  bool is_descriptor_directory(FileDescriptor fd) {
    return (fd.directory_plus_size >> 15) == 1;
  }
  
  uint16_t get_descriptor_size(FileDescriptor fd) {
    return fd.directory_plus_size & 0x7FFF;
  }

  // NOTE: F bit in file_size argument is ignored
  void set_directory_plus_size(FileDescriptor &fd, bool directory, uint16_t file_size) {
    fd.directory_plus_size = (directory << 15) | ((file_size << 1) >> 1);
  }

  // SIZE OF BUFFER MUST BE >= 2
  unsigned int get_directory_plus_size_uint8_buffer(FileDescriptor fd, uint8_t *uint8_buffer) {
    // double check mask used here
    
    uint8_buffer[0] = static_cast<uint8_t>((fd.directory_plus_size >> 8) & 0x00FF);
    uint8_buffer[1] = static_cast<uint8_t>((fd.directory_plus_size >> 0) & 0x00FF);

    return 2;
  }

  // SIZE OF BUFFER MUST BE >= FILE DESCRIPTOR SIZE (4 right now)
  FileDescriptor load_fd_from_buffer(uint8_t *buffer) {
    FileDescriptor return_value;

    Serial.print(buffer[0]);
    Serial.print("-");
    Serial.print(buffer[1]);
    Serial.print("-");
    Serial.print(buffer[2]);
    Serial.print("-");
    Serial.println(buffer[3]);

    return_value.file_name = buffer[0];
    return_value.first_block = buffer[1];
    return_value.directory_plus_size = static_cast<uint16_t>(buffer[2]) << 8 | (static_cast<uint16_t>(buffer[3]));

    return return_value;
  }

  void print_descriptor_to_serial(FileDescriptor file_descriptor) {
    if(descriptor_utils::is_descriptor_directory(file_descriptor)) {
      Serial.print("Directory-");
    } else {
      Serial.print("File-");
    }

    Serial.print(static_cast<char>(file_descriptor.file_name));
    Serial.print("-block ");
    Serial.print(file_descriptor.first_block);
    Serial.print("-size ");
    Serial.println(descriptor_utils::get_descriptor_size(file_descriptor));
  }
}

class BlockManager {
public:
  BlockManager(bool init_fs) {
    if(init_fs) {
      m_emulated_fat[FREE_BLOCK_LINKED_LIST_BLOCK_HEAD_LOC] = 16;
      m_emulated_fat[FREE_BLOCK_LINKED_LIST_BLOCK_TAIL_LOC] = 255;
      
      for(int i = 16; 256 > i; ++i) {
        if(i == 255) {
          m_emulated_fat[i] = EOF_FLAG;
        } else {
          m_emulated_fat[i] = i + 1;
        }
        
        if(i % 16 == 0) {
          update_data_block((i / 16) - 1);
        }
      }

      update_data_block(15);

      FileDescriptor root_fd;

      root_fd.file_name = ROOT_FD_NAME;
      descriptor_utils::set_directory_plus_size(root_fd, true, 0);

      m_emulated_fat[ROOT_FILE_DESCRIPTOR_LOC] = root_fd.file_name;
      m_emulated_fat[ROOT_FILE_DESCRIPTOR_LOC + 1] = root_fd.first_block;

      descriptor_utils::get_directory_plus_size_uint8_buffer(root_fd, &m_emulated_fat[ROOT_FILE_DESCRIPTOR_LOC + 2]);

      update_data_loc(ROOT_FILE_DESCRIPTOR_LOC);
    } else {
      #ifndef EMULATE_EEPROM
        Serial.println("Initial EEPROM read:");

        for(int i = 0; RESERVED_BLOCKS_LEN > i; ++i) {
          uint8_t page_buffer[BLOCK_SIZE_BYTES];

          read_block(i, &page_buffer[0]);

          for(int i2 = 0; BLOCK_SIZE_BYTES > i2; ++i2) {
            m_emulated_fat[(16*i) + i2] = page_buffer[i2];

            Serial.print(page_buffer[i2]);
            Serial.print(" ");
          }

          Serial.println();
        }
      #endif
    }
  }

  int allocate_block(unsigned int predessor_block_num = EOF_FLAG) {
    if(m_emulated_fat[FREE_BLOCK_LINKED_LIST_BLOCK_HEAD_LOC] == EOF_FLAG) {
      return -1;
    } else {
      int free_block = m_emulated_fat[FREE_BLOCK_LINKED_LIST_BLOCK_HEAD_LOC];

      Serial.print("old head: ");
      Serial.println(m_emulated_fat[FREE_BLOCK_LINKED_LIST_BLOCK_HEAD_LOC]);

      m_emulated_fat[FREE_BLOCK_LINKED_LIST_BLOCK_HEAD_LOC] = m_emulated_fat[free_block];

      Serial.print("new head: ");
      Serial.println(m_emulated_fat[FREE_BLOCK_LINKED_LIST_BLOCK_HEAD_LOC]);

      if(predessor_block_num != EOF_FLAG) {
        m_emulated_fat[predessor_block_num] = free_block;

        Serial.println("Updating predecessor");

        update_data_loc(predessor_block_num);
      }

      m_emulated_fat[free_block] = EOF_FLAG;

      update_data_loc(FREE_BLOCK_LINKED_LIST_BLOCK_HEAD_LOC);
      update_data_loc(free_block);
      
      return free_block;
    }
  }

  uint8_t get_tail_block(uint8_t head_block) const {
    uint8_t past_block;

    while(head_block != EOF_FLAG) {
      past_block = head_block;
      head_block = get_next_block_in_file(head_block);
    }

    return past_block;
  }

  uint8_t get_next_block_in_file(uint8_t previous_block) const {
    return m_emulated_fat[previous_block];
  }
  
  void free_block(unsigned int block_num, unsigned int predessor_block_num = EOF_FLAG) {   
    if(predessor_block_num != EOF_FLAG) {
      m_emulated_fat[predessor_block_num] = EOF_FLAG;
      
      update_data_loc(predessor_block_num);
    }

    // TODO(Bobby): Out of space Edge case

    Serial.print("reeeeeeeeeeee:");
    Serial.println(m_emulated_fat[FREE_BLOCK_LINKED_LIST_BLOCK_TAIL_LOC]);
    
    m_emulated_fat[m_emulated_fat[FREE_BLOCK_LINKED_LIST_BLOCK_TAIL_LOC]] = block_num;

    update_data_loc(m_emulated_fat[FREE_BLOCK_LINKED_LIST_BLOCK_TAIL_LOC]);
    
    m_emulated_fat[FREE_BLOCK_LINKED_LIST_BLOCK_TAIL_LOC] = block_num;
    
    m_emulated_fat[block_num] = EOF_FLAG;
    update_data_loc(block_num);
  }
  
  void read_block(uint8_t block_num, uint8_t *data_buffer) {
    unsigned int base_address = block_num * 16;

    for(int i = 0; BLOCK_SIZE_BYTES > i; ++i) {
      *(data_buffer + i) = EEPROM.read(base_address + i);
    }
  }
  
  void reset_blocks() {
    for (int i = 0 ; i < TOTAL_BLOCKS; i++) {
      #ifdef EMULATE_EEPROM
        m_emulated_fat[i] = 0;
      #else
        EEPROM.write(i, 0);
      #endif
    }
  }
  
  void write_block(unsigned int block_num, uint8_t *data_buffer, unsigned int len) {
    unsigned int base_address = block_num * 16;

    if(16u < len) {
      len = 16u;
    }

    Serial.print("Writing from ");
    Serial.print(base_address);
    Serial.print(" to ");
    Serial.println(base_address + 15);

    for(int i = 0; len > i; ++i) {
       EEPROM.write(base_address + i, *(data_buffer + i));
    }

    if(block_num < 16) {
      for(int i = 0; len > i; ++i) {
        m_emulated_fat[i + (block_num * BLOCK_SIZE_BYTES)] = *(data_buffer + i);
      }
    }
  }

  void print_fat_table() {
    Serial.println("FAT Table:");
    
    for(int r = 0; r < 16; ++r) {
      for(int c = 0; c < 16; ++c) {

        #ifdef EMULATE_EEPROM
          Serial.print(m_emulated_fat[(r * 16) + c]);
        #else
          Serial.print(EEPROM.read((r * 16) + c));
        #endif

        if(c < 15) {
          Serial.print("-");
        }
      }
      Serial.println();
    }
  }
private:
  void update_data_loc(unsigned int data_loc) {
    update_data_block(data_loc / BLOCK_SIZE_BYTES);
  }

  void update_data_block(unsigned int block_num) {
    #ifndef EMULATE_EEPROM
      uint8_t data_buffer[BLOCK_SIZE_BYTES];

      Serial.print("Updating: ");
      Serial.println(block_num);

      for(int i = 0; BLOCK_SIZE_BYTES > i; ++i) {
        data_buffer[i] = m_emulated_fat[i + (block_num * 16)];
      }
    
      write_block(block_num, data_buffer, 16);
    #endif
  }

  uint8_t m_emulated_fat[TOTAL_BLOCKS];
};

namespace descriptor_utils {
  void save_fd_to_block(BlockManager &block_manager, FileDescriptor fd, uint8_t block, uint8_t offset) {
    uint8_t block_data_buffer[BLOCK_SIZE_BYTES];

    block_manager.read_block(block, &block_data_buffer[0]);

    block_data_buffer[offset] = fd.file_name;
    block_data_buffer[offset + 1] = fd.first_block;

    get_directory_plus_size_uint8_buffer(fd, &block_data_buffer[offset + 2]);

    block_manager.write_block(block, &block_data_buffer[0], BLOCK_SIZE_BYTES);
  }
}


BlockManager *block_manager;

namespace file_utils {
  void update_dirty_bit(FileHandle &file_handle) {
    if(file_handle.block_modified) {
        Serial.print("writing to block num ");
        Serial.println(file_handle.current_file_pos_block_num);

        Serial.print("cache contents: ");

        for(int i = 0; BLOCK_SIZE_BYTES > i; ++i) {
          Serial.print(static_cast<char>(file_handle.current_block_data_cache[i]));
          Serial.print(",");
        }

        Serial.println();

        block_manager->write_block(file_handle.current_file_pos_block_num, &file_handle.current_block_data_cache[0], BLOCK_SIZE_BYTES);

        file_handle.block_modified = false;
    }
  }

  uint8_t get_next_byte(FileHandle &file_handle) {
    uint8_t offset = file_handle.file_byte_pos % (BLOCK_SIZE_BYTES + 1);

    ++file_handle.file_byte_pos;

    Serial.print("buffer contents:");
    for(int i = 0; BLOCK_SIZE_BYTES > i; ++i) {
      Serial.print(static_cast<char>(file_handle.current_block_data_cache[i]));
    }
    Serial.println();

    if(offset >= BLOCK_SIZE_BYTES) {
      Serial.print("Past block was ");
      Serial.println(file_handle.current_file_pos_block_num);

      update_dirty_bit(file_handle);

      file_handle.current_file_pos_block_num = block_manager->get_next_block_in_file(file_handle.current_file_pos_block_num);

      Serial.print("Next block is ");
      Serial.println(file_handle.current_file_pos_block_num);

      if(file_handle.current_file_pos_block_num == EOF_FLAG) {
        return 0;
      }

      block_manager->read_block(file_handle.current_file_pos_block_num, &file_handle.current_block_data_cache[0]);

      ++file_handle.file_byte_pos;

      Serial.println(static_cast<char>(file_handle.current_block_data_cache[0]));
      return file_handle.current_block_data_cache[0];
    } else {
      Serial.println(file_handle.current_block_data_cache[offset]);
      return file_handle.current_block_data_cache[offset];
    }
  }

  FileDescriptor get_parent_dir_descriptor(BlockManager &block_manager, FileInteger file_int) {
    uint8_t descriptor_buffer[BLOCK_SIZE_BYTES];
    uint8_t fd_byte_offset = static_cast<uint8_t>((file_int >> 8) & 0x000F);

    Serial.print("Loading descriptor at block num ");
    Serial.print(static_cast<uint8_t>((file_int >> 0) & 0x00FF));
    Serial.print(" and byte offset ");
    Serial.println(fd_byte_offset);

    block_manager.read_block(static_cast<uint8_t>((file_int >> 0) & 0x00FF), &descriptor_buffer[0]);

    return descriptor_utils::load_fd_from_buffer(&descriptor_buffer[fd_byte_offset]);
  }

  void update_parent_dir_descriptor(FileDescriptor &descriptor, FileInteger file_int) {
    Serial.print("presize: ");
    Serial.println(descriptor_utils::get_descriptor_size(descriptor));

    Serial.print("quantity to add:");
    Serial.println(descriptor_utils::get_descriptor_size(descriptor) + FILE_DESCRIPTOR_SIZE);

    descriptor_utils::set_directory_plus_size(descriptor, descriptor_utils::is_descriptor_directory(descriptor), descriptor_utils::get_descriptor_size(descriptor) + FILE_DESCRIPTOR_SIZE);
    descriptor_utils::save_fd_to_block(*block_manager, descriptor, static_cast<uint8_t>((file_int >> 0) & 0x00FF), static_cast<uint8_t>((file_int >> 8) & 0x000F));

    Serial.print("newsize: ");
    Serial.println(descriptor_utils::get_descriptor_size(descriptor));
  }

  void set_next_byte(FileHandle &file_handle, uint8_t byte) {
    FileDescriptor current_file_descriptor;
    uint8_t descriptor_buffer[BLOCK_SIZE_BYTES];
    uint8_t descriptor_block = static_cast<uint8_t>((file_handle.file_integer >> 0) & 0x00FF);
    uint8_t descriptor_offset = static_cast<uint8_t>((file_handle.file_integer >> 8) & 0x000F);

    uint8_t offset = file_handle.file_byte_pos % BLOCK_SIZE_BYTES;

    block_manager->read_block(descriptor_block, &descriptor_buffer[0]);

    current_file_descriptor = descriptor_utils::load_fd_from_buffer(&descriptor_buffer[descriptor_offset]);

    Serial.print(offset);
    Serial.print("-");
    Serial.println(static_cast<char>(byte));

    if(offset == 0 || file_handle.file_size == 0) {
      uint8_t precursor_block = file_handle.current_file_pos_block_num;

      if(file_handle.file_size == 0) {
        Serial.println("precursor block is EOF_FLAG");
        precursor_block = EOF_FLAG;
      } else {
        update_dirty_bit(file_handle);
      }

      file_handle.current_file_pos_block_num = block_manager->allocate_block(precursor_block);

      if(file_handle.file_size == 0) {
        current_file_descriptor.first_block = file_handle.current_file_pos_block_num;
        file_handle.current_file_first_block_num = current_file_descriptor.first_block;
      }

      block_manager->read_block(file_handle.current_file_pos_block_num, &file_handle.current_block_data_cache[0]);

      file_handle.current_block_data_cache[0] = byte;
    } else {
      file_handle.current_block_data_cache[offset] = byte;
    }

    file_handle.block_modified = true;
    ++file_handle.file_byte_pos;
    ++file_handle.file_size;

    descriptor_utils::set_directory_plus_size(current_file_descriptor, descriptor_utils::is_descriptor_directory(current_file_descriptor), file_handle.file_size);
    descriptor_utils::save_fd_to_block(*block_manager, current_file_descriptor, descriptor_block, descriptor_offset);
  }
  
  FileHandle create_file(char file_name, FileInteger parent_dir_file_integer, bool is_directory) {
    // TODO(Bobby): Handle out of space edge case

    FileDescriptor parent_dir_descriptor = get_parent_dir_descriptor(*block_manager, parent_dir_file_integer);
    FileHandle return_value;
    uint8_t descriptor_block;
    uint8_t descriptor_block_size;
    uint8_t descriptor_size = descriptor_utils::get_descriptor_size(parent_dir_descriptor);

    if(descriptor_size == 0 || descriptor_size > BLOCK_SIZE_BYTES - FILE_DESCRIPTOR_SIZE) {
      if(parent_dir_descriptor.first_block == EOF_FLAG) {
        descriptor_block = block_manager->allocate_block();

        parent_dir_descriptor.first_block = descriptor_block;
      } else {
        descriptor_block = block_manager->allocate_block(block_manager->get_tail_block(parent_dir_descriptor.first_block));
      }
      descriptor_block_size = 0;
    } else {
      descriptor_block = block_manager->get_tail_block(parent_dir_descriptor.first_block);
      descriptor_block_size = descriptor_size;
    }

    FileDescriptor file_descriptor;

    file_descriptor.file_name = file_name;
    file_descriptor.directory_plus_size = is_directory << 15;

    descriptor_utils::save_fd_to_block(*block_manager, file_descriptor, descriptor_block, descriptor_block_size);
    update_parent_dir_descriptor(parent_dir_descriptor, parent_dir_file_integer);

    return_value.file_integer = (descriptor_block << 0) | (descriptor_block_size << 8);
    return_value.open = true;

    return return_value;
  }

  FileHandle open_file(char file_name, FileInteger parent_dir_file_integer) {
    uint8_t block_buffer[BLOCK_SIZE_BYTES];
    bool found = false;
    FileDescriptor parent_dir_descriptor = get_parent_dir_descriptor(*block_manager, parent_dir_file_integer);
    FileHandle return_value;

    uint8_t current_block = parent_dir_descriptor.first_block;

    while(!found || current_block == EOF_FLAG) {
      uint8_t file_block_buffer[BLOCK_SIZE_BYTES];

      block_manager->read_block(current_block, &file_block_buffer[0]);

      for(int i = 0; BLOCK_SIZE_BYTES > i; i += FILE_DESCRIPTOR_SIZE) {
        // WE CANNOT HAVE A FILENAME BE '\0' (or ascii = 0)
        if(file_block_buffer[i] == file_name) {
          FileDescriptor current_descriptor = descriptor_utils::load_fd_from_buffer(&file_block_buffer[i]);

          return_value.current_file_pos_block_num = current_descriptor.first_block;

          Serial.print("start block = ");
          Serial.println(return_value.current_file_pos_block_num);

          block_manager->read_block(return_value.current_file_pos_block_num, &return_value.current_block_data_cache[0]);

          Serial.println("Loaded to buffer");

          return_value.file_size = descriptor_utils::get_descriptor_size(current_descriptor);
          return_value.file_integer = (current_block << 0) | (i << 8);
          return_value.open = true;

          Serial.print("File size set to ");
          Serial.println(return_value.file_size);

          if(return_value.file_size > 0) {
            return_value.current_file_first_block_num = return_value.current_file_pos_block_num;
          }

          found = true;

          break;
        }
      }

      if(!found) {
        current_block = block_manager->get_next_block_in_file(current_block);
      }
    }

    return return_value;
  }

  uint16_t read_file(FileHandle &file_handle, uint8_t *file_buffer, uint16_t buffer_size) {
    uint16_t bytes_to_read = min(buffer_size, file_handle.file_size - file_handle.file_byte_pos);

    Serial.print("Start block:");
    Serial.println(file_handle.current_file_pos_block_num);

    for(uint16_t i = 0; bytes_to_read > i; ++i) {
      file_buffer[i] = get_next_byte(file_handle);
    }

    Serial.print("Buffer size:");
    Serial.println(buffer_size);
    Serial.print("Actual bytes read:");
    Serial.println(bytes_to_read);

    return bytes_to_read;
  }

  uint16_t write_file(FileHandle &file_handle, uint8_t *file_buffer, uint16_t buffer_size) {
    // TODO(Bobby): implement available_free_space function to cap this quantitiy
    uint16_t bytes_to_write = buffer_size;

    for(uint16_t i = 0; bytes_to_write > i; ++i) {
      set_next_byte(file_handle, file_buffer[i]);
    }

    return bytes_to_write;
  }

  uint16_t seek_file(FileHandle &file_handle, uint8_t seek_pos) {
    uint16_t new_position = min(seek_pos, file_handle.file_size);

    Serial.print("New seek pos:");
    Serial.println(new_position);

    uint8_t new_block = seek_pos / BLOCK_SIZE_BYTES;

    if(file_handle.file_byte_pos / BLOCK_SIZE_BYTES != new_block) {
      update_dirty_bit(file_handle);

      file_handle.current_file_pos_block_num = file_handle.current_file_first_block_num;

      while(new_block > 0) {
        file_handle.current_file_pos_block_num = block_manager->get_next_block_in_file(file_handle.current_file_pos_block_num);

        --new_block;
      }

      block_manager->read_block(file_handle.current_file_pos_block_num, &file_handle.current_block_data_cache[0]);
    }

    file_handle.file_byte_pos = new_position;

    return new_position; 
  }

  uint16_t preallocate_bytes(FileHandle &file_handle, uint8_t num_of_bytes) {
    uint8_t filler_buffer[num_of_bytes];

    if(file_handle.open) {
      write_file(file_handle, filler_buffer, num_of_bytes);
      seek_file(file_handle, file_handle.file_byte_pos - num_of_bytes);
    }
  }

  void flush_file(FileHandle &file_handle) {
    update_dirty_bit(file_handle);
  }

  void close_file(FileHandle &file_handle) {
    flush_file(file_handle);

    file_handle.open = false;
  }
}

namespace directory_utils {
  FileInteger get_root_file_dir_integer() {
    return ROOT_FILE_DESCRIPTOR_LOC << 8;
  }

  void print_folder_file_descriptors(FileDescriptor folder_descriptor) {
    uint8_t buffer[BLOCK_SIZE_BYTES];

    Serial.println("Folder file descriptor:");

    descriptor_utils::print_descriptor_to_serial(folder_descriptor);

    uint16_t num_of_file_descriptors = 0;
    uint8_t block_bytes_read = 0;
    uint8_t block_num = folder_descriptor.first_block;
    uint16_t bytes_read = 0;
    uint16_t descriptor_size = descriptor_utils::get_descriptor_size(folder_descriptor);

    block_manager->read_block(block_num, &buffer[0]);

    while(bytes_read < descriptor_size) {
      if(block_bytes_read == 0 && bytes_read > 0) {
        block_num = block_manager->get_next_block_in_file(block_num);

        if(block_num == EOF_FLAG) {
          break;
        }

        block_manager->read_block(block_num, &buffer[0]);
      }

      FileDescriptor child_descriptor = descriptor_utils::load_fd_from_buffer(&buffer[block_bytes_read]);

      Serial.print("File descriptor #");
      Serial.println(num_of_file_descriptors);

      if(descriptor_utils::is_descriptor_directory(child_descriptor)) {
        print_folder_file_descriptors(child_descriptor);
      } else {
        descriptor_utils::print_descriptor_to_serial(child_descriptor);
      }

      ++num_of_file_descriptors;
      block_bytes_read += FILE_DESCRIPTOR_SIZE;
      bytes_read += FILE_DESCRIPTOR_SIZE;

      if(block_bytes_read >= BLOCK_SIZE_BYTES) {
        block_bytes_read = 0;
      }
    }
  }

  void print_root_folder_file_descriptors() {
    uint8_t buffer[BLOCK_SIZE_BYTES];

    block_manager->read_block(0, &buffer[0]);

    print_folder_file_descriptors(descriptor_utils::load_fd_from_buffer(&buffer[ROOT_FILE_DESCRIPTOR_LOC]));
  }
}

namespace handle_utils {
  /*uint8_t get_file_descriptor_block_from_handle(FileHandle fh) {
    return fh
  }*/

  void print_handle_to_serial(FileHandle &handle) {
    if(handle.open) {
      Serial.print("Open-");
    } else {
      Serial.print("Closed-");
    }

    if(handle.block_modified) {
      Serial.print("Modified Block-");
    } else {
      Serial.print("Unmodified Block-");
    }

    Serial.print("size ");
    Serial.print(handle.file_size);

    Serial.print("-byte pos ");
    Serial.print(handle.file_byte_pos);

    Serial.print("-block num ");
    Serial.println(handle.current_file_pos_block_num);
  }
}

void setup() {
  Serial.begin(9600);

  block_manager = new BlockManager(false);

  Serial.println("Awaiting command");
}

int obtain_num(char *advisory_message) {
  while(Serial.available() > 0) {
    Serial.read();
  }
  
  Serial.println(advisory_message);

  while(true) {
    if(Serial.available() > 0) {
      int nextInt = Serial.parseInt();

      if(nextInt >= 16) {
        return nextInt;
      }
    }
  }
}

FileHandle current_file_handle;
FileHandle current_directory_handle;

void loop() {
  if(Serial.available() > 0) {
      char current_char = Serial.read();

      Serial.println(current_char);
      
      switch(current_char) {
        case 'p':
        {
          block_manager->print_fat_table();
          break;
        }
        case 'a':
        {
          Serial.print("Allocated Block #");
          Serial.println(block_manager->allocate_block());
          break;
        }
        case 'i':
        {
          delete block_manager;
          
          block_manager = new BlockManager(true);

          Serial.println("inited FS");
          
          break;
        }
        case 'w':
        {
          int write_block_num = obtain_num("Enter Block #");
          uint8_t data[16] = {'g', 'a', 'm', 'i', 'n', 'g', 't', 'i', 'm', 'e', 'w', 'o', '0', 'o', 0, '0'};

          block_manager->write_block(write_block_num, data, 16);

          Serial.print("Wrote Block #");
          Serial.println(write_block_num);
          
          break;
        }
        case 'f':
        {
          int block_num = obtain_num("Enter Block #");

          block_manager->free_block(block_num);

          Serial.print("Freed Block #");
          Serial.println(block_num);
          
          break;
        }
        case 'r':
        {
          int read_block_num = obtain_num("Enter Block #");
          uint8_t data[16];

          block_manager->read_block(read_block_num, &data[0]);

          Serial.print("Read Block #");
          Serial.println(read_block_num);
          Serial.println("Contents:");

          for(int i = 0; i < 16; ++i) {
            Serial.print(static_cast<char>(data[i]));

            if(i < 15) {
              Serial.print("-");
            }
          }
          
          break;
        }
        case '1':
        {
          // creates file
          if(current_file_handle.open) {
            Serial.println("Current file handle is already open!");
          } else {
            current_file_handle = file_utils::create_file('A', directory_utils::get_root_file_dir_integer(), false);
          }

          break;
        }
        case '2':
        {
          // opens file
          if(current_file_handle.open) {
            Serial.println("Current file handle is already open!");
          } else {
            current_file_handle = file_utils::open_file('A', directory_utils::get_root_file_dir_integer());
          }

          break;
        }
        case '3':
        {
          // closes file
          if(current_file_handle.open) {
            Serial.println("Closing current file handle");

            file_utils::close_file(current_file_handle);

            Serial.println("Closed current file handle");
          } else {
            Serial.println("Current file handle is not open");
          }
          break;
        }
        case '4':
        {
          // reads file
          if(current_file_handle.open) {
            uint16_t entire_file_size = current_file_handle.file_size;

            uint8_t entire_file_buffer[entire_file_size];

            file_utils::seek_file(current_file_handle, 0);
            file_utils::read_file(current_file_handle, entire_file_buffer, entire_file_size);

            Serial.println("File contents:");

            for(int i = 0; entire_file_size > i; ++i) {
              Serial.print(static_cast<char>(entire_file_buffer[i]));
            }

            Serial.println();
          } else {
            Serial.println("Current file handle is not open");
          }
          break;
        }
        case '5':
        {
          // writes file
          if(current_file_handle.open) {
            uint8_t file_buffer[27] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '\0'};

            file_utils::seek_file(current_file_handle, 0);
            Serial.println(file_utils::write_file(current_file_handle, file_buffer, 27));

            Serial.println("written 27 char buffer into file");
          } else {
            Serial.println("Current file handle is not open");
          }
          break;
        }
        case '6':
        {
          // creates directory

          current_directory_handle = file_utils::create_file('D', directory_utils::get_root_file_dir_integer(), true);

          Serial.println("Directory created");

          break;
        }
        case '7':
        {
          // creates file in directory 'D'
          if(current_directory_handle.open) {
            current_file_handle = file_utils::create_file('W', current_directory_handle.file_integer, false);

            Serial.println("File in directory created.");
          } else {
            Serial.println("Directory not open!");
          }

          break;
        }
        case '8':
        {
          // opens file 'W' in directory 'D'
          uint8_t descriptor_buffer[BLOCK_SIZE_BYTES];

          current_directory_handle = file_utils::open_file('D', directory_utils::get_root_file_dir_integer());

          Serial.println("directory opened");

          current_file_handle = file_utils::open_file('W', current_directory_handle.file_integer);

          Serial.println("file opened");
          break;
        }
        case 'd':
          // descriptor debug
          Serial.println("Printing all descriptors of the root directory:");

          directory_utils::print_root_folder_file_descriptors();

          break;
        case 'h':
          // handle debug
          Serial.println("Printing current file handle");

          handle_utils::print_handle_to_serial(current_file_handle);

          break;
        case '\n':
          return;
        default:
          Serial.println("Unknown Command");
      }

      //Serial.println("Awaiting command");
  }
}
